﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    interface ISquareCalculattable
    {
        double Area();
    }

    class Rectangle : ISquareCalculattable
    {
        private double width;
        private double height;
        public double Width
        {
            get
            {
                return width;
            }

            set
            {
               if(value<=0)
               {
                   throw new Exception("error");
               }
               width = value;
            }
          
        }

        public double Height
        {
            get
            {
                return height;
            }

            set
            {
                if (value <= 0)
                {
                    throw new Exception("error");
                }
                height = value;
            }

        }

        public double Area()
        {
            return width * height;

            //calculates area of a rectangle
        }
        public override string ToString()
        {
            return Area().ToString();
        }
    }

    class Triangle:ISquareCalculattable
    {
        private double a;
        private double b;
        private double c;
        public double A
        {
            get
            {
                return a;
            }
            set
            {
                if(value<=0)
                {
                    throw new Exception("error");
                }
                a = value;
            }
        }

        public double B
        {
            get
            {
                return b;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("error");
                }
                b = value;
            }
        }

        public double C
        {
            get
            {
                return c;
            }
            set
            {
                if (value <= 0)
                {
                    throw new Exception("error");
                }
                c = value;
            }
        }

        public double Area()
        {
            double p=(a+b+c)/2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c)); 

            //calculates area of a rectangle using its semiperimeters

        }

        public override string ToString()
        {
            return Area().ToString();
        }
   
    }

    class Circle : ISquareCalculattable
    {
        private double r;
        public double R
        {
            get
            {
                return r;
            }
            set
            {
                if(value<=0)
                {
                    throw new Exception("error");
                }
                r = value;
            }
        }

        public double Area()
        {
            return Math.PI * r * r;

            //calculates area of a circle
        }
        public override string ToString()
        {
            return Area().ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("1 - triangle, 2 - rectangle, 3 - circle");
            Console.WriteLine();
            int v = int.Parse(Console.ReadLine());
         
                try
                {
                    if (v == 1)
                    {
                        Console.WriteLine("side A of the triangle: ");
                        Triangle tr = new Triangle();
                        tr.A = int.Parse(Console.ReadLine());
                        Console.WriteLine("side B of the triangle: ");
                        tr.B = int.Parse(Console.ReadLine());
                        Console.WriteLine("side C of the triangle: ");
                        tr.C = int.Parse(Console.ReadLine());
                        Console.WriteLine(tr.Area());
                    }

                    if (v == 2)
                    {
                        Console.WriteLine("height of the rectangle: ");
                        Rectangle r = new Rectangle();
                        r.Height = int.Parse(Console.ReadLine());
                        Console.WriteLine("width of the rectangle: ");
                        r.Width = int.Parse(Console.ReadLine());
                        Console.WriteLine(r.Area());
                    }

                    if(v==3)
                    {
                        Circle c = new Circle();
                        Console.WriteLine("raduis: ");
                        c.R = int.Parse(Console.ReadLine());
                        Console.WriteLine(c.Area());
                    }


                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            
            Console.ReadKey();


        }
    }
}
